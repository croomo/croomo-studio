<?php defined( 'ABSPATH' ) or die;

class Webforms_Shortcode
{
    public static function init()
    {
        add_shortcode('webform', array(__CLASS__, 'handle_shortcode'));
        add_action('init', array(__CLASS__, 'register_script'));
    }


    public static function handle_shortcode($atts)
    {
        $referrer = get_site_url();
        $variables = array(
            '{{webform_url}}' => esc_url($atts['url']),
            '{{webform_id}}' => 'webform-' . uniqid(),
            '{{brand_name}}' => WEBFORMS_BRANDING,
            '{{brand_url}}' => WEBFORMS_SYSTEM_URL,
            '{{brand_website}}' => WEBFORMS_BRAND_WEBSITE,
            '{{referrer}}' => $referrer
        );


        // if the user inputted url is not valid,
        // then throw the hacker outta here
        if(!$variables['{{webform_url}}']) {
            return '';
        }

        // check for template in active theme
        $template = locate_template(array(WEBFORMS_TEMPLATE));

        // if none found use the default template
        if ( $template == '' ) {
            $template =  WEBFORMS_TEMPLATE_PATH . WEBFORMS_TEMPLATE;
        }

        $output = str_replace(array_keys($variables), array_values($variables), file_get_contents($template));
        return $output;
    }


    public static function register_script()
    {
        wp_register_script('webform', WEBFORMS_IFRAME_RESIZER_SCRIPT, false, false, true);
        wp_enqueue_script('webform');
    }
}