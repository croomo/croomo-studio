<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package croomo
 */

get_header(); ?>

<section class="single-post">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', get_post_format() );

					pagination_bar();
					//the_post_navigation();

				endwhile; // End of the loop.
				?>
			</div>
			<div class="sidebar col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php 



echo array_search(get_post(), get_posts($args));

 ?>

<div class="parallax-window text-center" data-bleed="1" data-parallax="scroll" data-image-src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/02/single-blog-read-more-bg.png">
	<div class="parallax-overlay" style="background: rgba(105, 14, 103, 0.75)"></div>
	<div class="parallax-content">
		<h1 class="lime-green">Read More!</h1>
		<a href="<?php generateGoBackToBlogURL(); ?>" class="button white">Take me back to blog posts!</a>
	</div>
</div>


<?php

function generateGoBackToBlogURL() {
	// Setup vars
	$getPostsArgs = array('numberposts' => -1);
	$postArray = get_posts($getPostsArgs);
	$currentPost = get_post();

	$postIndex = array_search($currentPost, $postArray);

	// Find the appropriate page for the current post
	$postsShownPerPage = get_option( 'posts_per_page' );
	if ($postIndex > $postsShownPerPage) {
		$pageIndex = floor($postIndex / $postsShownPerPage) + 1;
		$pageNumberURL = "/page/".$pageIndex."/";
	}
	else
		$pageNumberURL = "";

	echo get_site_url().'/blog'.$pageNumberURL.'#post-'.$currentPost->ID;
}

get_footer();
