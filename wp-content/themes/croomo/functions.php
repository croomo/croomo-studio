<?php
/**
 * croomo functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package croomo
 */

if(is_user_logged_in()) {

	$inDevelopment = true;

	$hostname = 'croomo.online';
	$dbname = 'croomoon_websitestatus';
	$username = 'croomoon_croomo';
	$password = 'Tg!boDaa&!a8';
	$serverName = $_SERVER['SERVER_NAME'];

	$mysqlConnect = new mysqli($hostname, $username, $password, $dbname);

	if($mysqlConnect->connect_error) {
		die('Connection failed: ' . $mysqlConnect->connect_error);
	} else {

		$getSQL = "SELECT in_development FROM status WHERE id = 1";
		$result = $mysqlConnect->query($getSQL);

		if($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$inDevStatus = $row['in_development'];
			if($inDevStatus == 0) {
				$inDevStatus = false;
			} else {
				$inDevStatus = true;
			}
		} else {
			echo 'No results';
		}

		if($serverName == '192.168.5.30') {

			$updateSQL = "UPDATE status SET in_development = '{$inDevelopment}' WHERE id = 1";

			// check if inDevelopment status is different from status in the DB, if it is update the DB
			if($inDevelopment != $inDevStatus ) {
				if($mysqlConnect->query($updateSQL) === true) {
					//echo 'updated successfully';
					$inDevStatus = $inDevelopment;
				} else {
					echo 'Error: ' . $updateSQL . '<br>' . $mysqlConnect->error;
				}
			}
		}

		$mysqlConnect->close();

		// If developing, is set to 1 in DB
		if($inDevStatus == true) {		
			if($serverName == 'croomo.com') {
				// append class to body
				add_filter('body_class', function($classes) {
					return array_merge($classes, array('in-development'));
				});
				// append class to body wp admin
				add_filter('admin_body_class', function($classes) {
					return 'in-development';
				});
			}
		} else {
			if($serverName == '192.168.5.30') {
				// append class to body
				add_filter('body_class', function($classes) {
					return array_merge($classes, array('not-in-development'));
				});
				// append class to body wp admin
				add_filter('admin_body_class', function($classes) {
					return 'not-in-development';
				});
			}
		}

		function add_toolbar_items($wp_admin_bar) {
			global $inDevStatus;

			if($inDevStatus == true) {
				$title = 'Developing...';	
				$class = 'in-development';
			} else {
				$title = 'Not Developing';
				$class = 'not-developing';
			}	

		    $wp_admin_bar->add_menu( array(
		        'id'    => 'in-development',
		        'title' => $title,
		        'href'  => 'javascript:void(0)',
		        'meta'  => array(
		            'title' => $title,
		            'class' => $class          
		        ),
		    ));
		}
		add_action('admin_bar_menu', 'add_toolbar_items', 100);		
	}
}


if ( ! function_exists( 'croomo_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function croomo_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on croomo, use a find and replace
	 * to change 'croomo' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'croomo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'croomo' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'croomo_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'croomo_setup' );


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Global Settings',
		'menu_title'	=> 'Global Settings',
		'menu_slug' 	=> 'global-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function croomo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'croomo_content_width', 640 );
}
add_action( 'after_setup_theme', 'croomo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function croomo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'croomo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'croomo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'croomo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function croomo_scripts() {
	wp_enqueue_style( 'croomo-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'croomo-flexslider', get_template_directory_uri() . '/css/flexslider.css' );
	wp_enqueue_style( 'croomo-fancybox', get_template_directory_uri() . '/js/fancybox/source/jquery.fancybox.min.css' );
	wp_enqueue_style( 'croomo-jquery-ui', get_template_directory_uri() . '/css/jquery-ui.min.css' );
	wp_enqueue_style( 'croomo-style', get_stylesheet_uri() );

	wp_enqueue_script( 'croomo-jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '20161027', true );
	wp_enqueue_script( 'croomo-jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', array(), '20161027', true );
	wp_enqueue_script( 'croomo-jquery-easing', get_template_directory_uri() . '/js/jquery.easing.min.js', array(), '20171027', true );
	wp_enqueue_script( 'croomo-waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array(), '20171029', true );
	wp_enqueue_script( 'croomo-debounced', get_template_directory_uri() . '/js/jquery.debouncedresize.js', array(), '20171028', true );
	wp_enqueue_script( 'croomo-parallax', get_template_directory_uri() . '/js/parallax.min.js', array(), '20161027', true );
	wp_enqueue_script( 'croomo-mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel.min.js', array(), '20170528', true );
	wp_enqueue_script( 'croomo-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array(), '20161027', true );
	wp_enqueue_script( 'croomo-skrollr', get_template_directory_uri() . '/js/skrollr.js', array(), '20170624', true );
	wp_enqueue_script( 'croomo-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '20170927', true );
	wp_enqueue_script( 'croomo-clipboard', get_template_directory_uri() . '/js/clipboard.min.js', array(), '20170928', true );
	wp_enqueue_script( 'croomo-infinite-scroll', get_template_directory_uri() . '/js/infinite-scroll.pkgd.min.js', array(), '20171003', true );
	//wp_enqueue_script( 'croomo-masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array(), '20171005', true );

	wp_enqueue_script( 'croomo-font-awesome', 'https://use.fontawesome.com/f25a2d8a13.js', array(), '20161028', true);
	wp_enqueue_script( 'croomo-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20161027', true );
	wp_enqueue_script( 'croomo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'croomo-aframe', 'https://aframe.io/releases/0.7.0/aframe.min.js', array(), '20151215', true );
	wp_enqueue_script( 'croomo-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCm7THlkV5Jr5-2fm35hY95x61A78y7Z-k', array(), '20161215', true );
	wp_enqueue_script( 'croomo-fancybox', get_template_directory_uri() . '/js/fancybox/source/jquery.fancybox.min.js', array(), '20161215', true );

	wp_enqueue_script( 'croomo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'croomo-scripts', get_template_directory_uri() . '/js/scripts.js', array(), '20161027', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'croomo_scripts' );

// Update CSS and JS within in Admin
function croomo_admin_scripts() {
	wp_enqueue_style('admin-styles', get_template_directory_uri().'/css/admin.css');
	wp_enqueue_script('admin-scripts', get_template_directory_uri().'/js/admin.js');
}
add_action('admin_enqueue_scripts', 'croomo_admin_scripts');

function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyCm7THlkV5Jr5-2fm35hY95x61A78y7Z-k');
}

add_action('acf/init', 'my_acf_init');

function pagination_bar() {
    global $wp_query;

    $total_pages = $wp_query->max_num_pages;

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Creates the html required for a signup form to be used on the main blog page.
 */

function generateSignupForm() {
	echo '<div class="signup-container"><h3 class="text-center">Sign up to receive updates from Croomo</h3>
	<form method="POST" class="em_wfe_form" id="" name="" action="https://www.vision6.com.au/em/forms/subscribe.php?db=405825&s=141301&a=69750&k=a9af041&va=5" enctype="multipart/form-data">
		<input type="hidden" name="webform_submit_catch" value="" />
		<input type="hidden" name="sf_catch" value="" />
		<div rel="5" class="wfe_component" id="1526351">
			<input type="text" value="" id="em_wfs_formfield_3613262" name="em_wfs_formfield_3613262" validation="" ftype="1" maxlength=255 size=30 placeholder="First Name" />
		</div>
		<div rel="5" class="wfe_component" id="1526350">
			<input type="text" value="" id="em_wfs_formfield_3613263" name="em_wfs_formfield_3613263" validation="" ftype="1" maxlength=255 size=30 placeholder="Last Name" />
		</div>
		<div rel="5" class="wfe_component" id="1526355">
			<input type="text" value="" id="em_wfs_formfield_3613261" name="em_wfs_formfield_3613261" validation="1" ftype="1" maxlength=255 size=30 placeholder="Email Address" />
		</div>
		<div rel="3" class="wfe_component" id="1526349">
			<div align="left">
				<input class="wfe_button" type="submit" title="Submit" value="Submit" />
			</div>
		</div>
		<!-- Do not remove, as this DIV and INPUT help fight against spam. -->
		<div style="display:none;position:absolute;left:-10000px;top:-10000px;">
			<label for="webform_5abf3ef" style="display:none;visibility:hidden;width:0px;height:0px;font-size:1px;">Ignore</label>
			<input type="text" id="webform_5abf3ef" name="webform_5abf3ef" value="" style="display:none;visibility:hidden;width:0px;height:0px;font-size:1px;" />
		</div>
	</form></div>';
}

function php_execute($html) {
	if(strpos($html,"<"."?php")!==false) {
		ob_start();
		eval("?".">".$html);
		$html=ob_get_contents();
		ob_end_clean();
	}
	return $html;
}

add_filter('widget_text','php_execute',100);
