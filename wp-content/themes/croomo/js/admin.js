jQuery(document).ready(function($){
	if($('body').hasClass('in-development')) {
        alert('Looks like the croomo website IS in development mode, we\'ll redirect you to the local environment so you can make your changes there.');
        window.location.href = 'http://192.168.5.30/croomo-website/wp-admin';
    } else if($('body').hasClass('not-in-development')) {
         alert('Looks like the croomo website is NOT in development mode, we\'ll redirect you to the live environment so you can make your changes there.');
         window.location.href = 'http://croomo.com/wp-admin';
    }
});