<?php defined( 'ABSPATH' ) or die;

require_once(WEBFORMS_INC_PATH . 'shortcode.php');
require_once(WEBFORMS_INC_PATH . 'widget.php');

// Register and load the widget
Webforms_Widget::init();

// register and load the webforms shortcode
// [webform url=http://shortcode.com/id]
Webforms_Shortcode::init();

// Allow shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');
