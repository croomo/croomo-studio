<?php defined( 'ABSPATH' ) or die;

// Creating the widget
class Webforms_Widget extends WP_Widget
{
    /**
     * add action for this widget
     */
    public static function init()
    {
        add_action( 'widgets_init', 'Webforms_Widget::register');
    }


    /**
     * Register this widget class with the WordPress system
     */
    public static function register()
    {
        register_widget(__CLASS__);
    }


    /**
     * Standard wordpress constructor
     */
    function __construct()
    {
        parent::__construct(
        // Base ID of your widget
        'webform_widget',

        // Widget name will appear in UI
        __(WEBFORMS_BRANDING . ' web forms', 'webform_widget_domain'),

        // Widget description
        array( 'description' => __( 'A web form from ' . WEBFORMS_BRANDING, 'webform_widget_domain' ), )
        );
    }


    /**
     * Renders the widget
     *
     * @param  array $args      Wordpress arguments
     * @param  array $instance  These are the widget values
     */
    public function widget( $args, $instance )
    {
        $title_text = array_key_exists('title', $instance) ? $instance['title'] : '';
        $title = apply_filters( 'widget_title', $title_text);

        $webform_url = array_key_exists('webform_url', $instance) ? $instance['webform_url'] : '';
        // if there is no webform_url then get outta here
        if(!$webform_url) { return; }


        $referrer = get_site_url();
        $variables = array(
            '{{webform_url}}' => esc_url($webform_url),
            '{{webform_id}}' => 'webform-' . uniqid(),
            '{{brand_name}}' => WEBFORMS_BRANDING,
            '{{brand_url}}' => WEBFORMS_SYSTEM_URL,
            '{{brand_website}}' => WEBFORMS_BRAND_WEBSITE,
            '{{referrer}}' => $referrer
        );


        // if the user inputted url is not valid,
        // then thow the hacker outta here
        if(!$variables['{{webform_url}}']) {
            return '';
        }

        // check for template in active theme
        $template = locate_template(array(WEBFORMS_TEMPLATE));

        // if none found use the default template
        if ( $template == '' ) {
            $template =  WEBFORMS_TEMPLATE_PATH . WEBFORMS_TEMPLATE;
        }

        $output = $args['before_widget'];
        if (!empty($title)) {
            $output .= $args['before_title'] . $title . $args['after_title'];
        }
        $output .= str_replace(array_keys($variables), array_values($variables), file_get_contents($template));
        $output .= $args['after_widget'];

        echo $output;
    }


    // Widget Backend
    public function form( $instance )
    {
        $title = (isset( $instance[ 'title' ])) ? $instance[ 'title' ] : '';
        $webform_url = (isset($instance['webform_url'])) ? $instance['webform_url'] : '';

        // Widget admin form
        include(WEBFORMS_PLUGIN_PATH . 'admin/widget_form.php');
    }


    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance )
    {
        $instance = array();

        $instance['title'] = (!empty( $new_instance['title']))
            ? strip_tags($new_instance['title'])
            : '';

        // if the user enters the shortcode or the url this should get the correct one.
        $webform_url = '';
        if(!empty( $new_instance['webform_url'])) {
            $webform_url_matches = wp_extract_urls($new_instance['webform_url']);
            $webform_url = (count($webform_url_matches) > 0)
                ? esc_url($webform_url_matches[0])
                : '';
        }

        $instance['webform_url'] = $webform_url;

        return $instance;
    }
}