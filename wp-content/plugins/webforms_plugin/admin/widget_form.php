<?php defined('ABSPATH') or die; ?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title: (Leave empty for none)' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'webform_url' ); ?>"><?php _e( 'Web form URL or Shortcode:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'webform_url' ); ?>" name="<?php echo $this->get_field_name( 'webform_url' ); ?>" type="text" value="<?php echo esc_attr( $webform_url ); ?>" />
</p>
