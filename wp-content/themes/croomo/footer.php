<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package croomo
 */

?>
	<div id="visible-footer-content">
		<?php if(!is_page_template('page-projects.php') && !is_page_template('page-contact.php') && !is_page_template('page-squeeze.php') && !is_page_template('page-client-videos.php') && !is_home() && !is_single() && !is_404()): ?>
			<!-- CONTACT FORM -->
			<?php
				$getBGImage = get_field('contact_form_background', 'option');
			?>
			<section class="contact-form" style="background-image: url(<?php echo $getBGImage['url']; ?>);">
				<div class="container text-center">
					<div class="row">
						<h1 id="contact-title">
							<?php
							if(is_page_template('page-adhoc.php')) {
								echo 'To receive more information or register your interest, please submit your details below.';
							} else if(is_page_template('page-studio-hire.php')) {
								echo 'I need the works';
							} else {
								the_field('contact_form_title', 'option');
							} ?>
						</h1>
						<?php 
						if(is_page_template('page-studio-hire.php')): ?>
							<div class="contact-description">
								<p>You’ve got an idea for an amazing project, but you don’t know how to make it a reality. We can help. Get in touch, and we’ll figure out how to make your plan come together.</p>
							</div>
						<?php 
						endif; ?>
						<div class="col-xs-12 contact-container">
							<?php the_field('contact_form', 'option'); ?>
						</div>
					</div>
				</div>
			</section>
			<!-- END CONTACT FORM -->
		<?php endif; ?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container">
				<div class="col-xs-12 col-md-2">
					<p><?php the_field('column_1', 'option'); ?></p>
				</div>
				<div class="col-xs-12 col-md-4">
					<p><?php the_field('column_2', 'option'); ?></p>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="pull-right">
						<p>&copy; Copyright <?php echo date("Y"); ?> <?php the_field('column_3', 'option'); ?></p>
						<?php
						$getIcon = get_field('footer_icon', 'option');
						if(!empty($getIcon)): ?>
							<img src="<?php echo $getIcon['url']; ?>" alt="<?php echo $getIcon['alt']; ?>" title="<?php echo $getIcon['alt']; ?>" />
						<?php endif; ?>
					</div>
				</div>
			</div>
		</footer><!-- #colophon -->
	</div>
</div><!-- #page -->

<?php
if ( !is_front_page() && is_home() ) :
?>
<div class="popup">
	<!-- Web Form Code -->
	<div class="form-wrapper center-vertically">
		<?php generateSignupForm(); ?>
	</div>
	<div class="close-popup"></div>
</div>
<?php endif ?>

<?php wp_footer(); ?>

<!-- Google Code for Remarketing Tag -->
<div style="display:none;">
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 852991730;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
</div>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/852991730/?guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>
