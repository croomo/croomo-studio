<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package croomo
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="google-site-verification" content="DjAl-NUOp9oXjgDkv9PpuAf0VwvrjTUr2gnIFAnocb0" />

<!-- Page hiding snippet (recommended) -->
<style>.async-hide { opacity: 0 !important} </style>
<script>
(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);
})(window,document.documentElement,'async-hide','dataLayer',2000,{'GTM-P6HRGX5':true});
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P6HRGX5');</script>
<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

</head>

<body <?php body_class();  echo ($_GET['r'] == 'elearningco/' ? 'data-referer="elearningco"' : '') . (is_user_logged_in() ? 'data-logged-in="yes"' : 'data-logged-in="no"'); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P6HRGX5"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Google Code for Form Submit Conversion Page
	In your html page, add the snippet and call
	goog_report_conversion when someone clicks on the
	chosen link or button. -->
	<script type="text/javascript">
	  /* <![CDATA[ */
	  goog_snippet_vars = function() {
	    var w = window;
	    w.google_conversion_id = 852991730;
	    w.google_conversion_label = "PM6LCIah-nQQ8r3elgM";
	    w.google_remarketing_only = false;
	  }
	  // DO NOT CHANGE THE CODE BELOW.
	  goog_report_conversion = function(url) {
	    goog_snippet_vars();
	    window.google_conversion_format = "3";
	    var opt = new Object();
	    opt.onload_callback = function() {
	    if (typeof(url) != 'undefined') {
	      window.location = url;
	    }
	  }
	  var conv_handler = window['google_trackConversion'];
	  if (typeof(conv_handler) == 'function') {
	    conv_handler(opt);
	  }
	}
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>

	<div id="referer-overlay">
		<div class="referer-overlay-content text-center">
			<span class="close-referer"><i class="fa fa-times" aria-hidden="true"></i></span>
			<?php
			$getLogo = get_field('logo', 'option');
			if(!empty($getLogo)): ?>
			<div class="logos-container">
				<img class="logo" src="<?php echo $getLogo['url']; ?>" alt="<?php echo $getLogo['alt']; ?>" title="<?php echo $getLogo['alt']; ?>" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/elearningco-logo.png" alt="The e-Learning Company" title="The e-Learning Company" />
			</div>
			<?php endif; ?>
			<div class="referer-content-container">
				<h2>We’ve got big news…</h2>
				<p>The e-Learning Company is now part of Croomo!</p>
				<p>Croomo is recognised as a great Brisbane business success story doing amazing work across the country. By joining forces we can offer you, our valued client, an unparalleled level of products and services. </p>
				<p>The team at The e-Learning Company thank you for your business, friendship and support and look forward to impressing you even more as part of the Croomo team.</p>
			</div>
		</div>
	</div>

	<div class="fixed-social-sidebar">
		<?php
			$socialButton1Img = get_field('social_button_1_image', 'option');
			$socialButton2Img = get_field('social_button_2_image', 'option');
			$socialButton3Img = get_field('social_button_3_image', 'option');
			$socialButton4Img = get_field('social_button_4_image', 'option');
			$socialButton5Img = get_field('social_button_5_image', 'option');
		?>
		<ul>
			<li><a href="<?php the_field('social_button_1_link', 'option'); ?>" target="_blank"><img src="<?php echo $socialButton1Img['url']; ?>" title="<?php echo $socialButton1Img['alt']; ?>" alt="<?php echo $socialButton1Img['alt']; ?>" /></a></li>
			<li><a href="<?php the_field('social_button_2_link', 'option'); ?>" target="_blank"><img src="<?php echo $socialButton2Img['url']; ?>" title="<?php echo $socialButton2Img['alt']; ?>" alt="<?php echo $socialButton2Img['alt']; ?>" /></a></li>
			<li><a href="<?php the_field('social_button_3_link', 'option'); ?>" target="_blank"><img src="<?php echo $socialButton3Img['url']; ?>" title="<?php echo $socialButton3Img['alt']; ?>" alt="<?php echo $socialButton3Img['alt']; ?>" /></a></li>
			<li><a href="<?php the_field('social_button_4_link', 'option'); ?>" target="_blank"><img src="<?php echo $socialButton4Img['url']; ?>" title="<?php echo $socialButton4Img['alt']; ?>" alt="<?php echo $socialButton4Img['alt']; ?>" /></a></li>
			<li><a href="<?php the_field('social_button_5_link', 'option'); ?>" target="_blank"><img src="<?php echo $socialButton5Img['url']; ?>" title="<?php echo $socialButton5Img['alt']; ?>" alt="<?php echo $socialButton5Img['alt']; ?>" /></a></li>
		</ul>
	</div>

	<div class="scroll-btn text-center">
		<i class="fa fa-angle-up" aria-hidden="true"></i>
	</div>

	<div id="page" class="site">

		<header id="masthead" class="site-header" role="banner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-8 col-sm-2">
						<?php
						$getLogo = get_field('logo', 'option');
						if(!empty($getLogo)): ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img class="logo" src="<?php echo $getLogo['url']; ?>" alt="<?php echo $getLogo['alt']; ?>" title="<?php echo $getLogo['alt']; ?>" /></a>
						<?php endif; ?>
					</div>
					<div class="col-xs-4 col-sm-10 col-lg-8">
						<nav id="site-navigation" class="main-navigation" role="navigation">
							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i></button>
							<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
						</nav><!-- #site-navigation -->
					</div>
					<div class="col-xs-12 col-md-3 col-lg-2 pull-right green-btn-container">
						<a href="<?php echo get_permalink(15); ?>" class="green-btn"><?php the_field('header_button', 'option'); ?></a>
					</div>
				</div>
			</div>
		</header><!-- #masthead -->

		<div id="content" class="site-content">
