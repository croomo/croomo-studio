jQuery(document).ready(function($){

    // Global variables
    var scrolling;
    var scrollingCount = 1;
    var map = null;
    var opacityCount = 0.003;
    var opacityCount2 = 1;

    // 02 Aug 2017: [Dylan] Commented this out - keep an eye out if this breaks anything
    //$(document).scrollTop(0);

    $('.acf-map').each(function(){
		// create map
		map = new_map( $(this) );

	});

    // Scroll Button
    $('.scroll-btn').click(function(){
        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });

    // ================================
    // Add-on capabilities popup code
    // ================================

    // Close any opened add-on capabilities if anything is clicked
    $(document).click(function() {
        if ($('.capabilites-container h4').next().hasClass('active')) {
            $('.capabilites-container h4').next().slideUp(200).removeClass('active');
        }
    });

    // Pop up an explanation of an add-on capability if it is clicked
    $('.add-on-capability-clickable').click(function(){
        if(!$(this).find('h4').next().hasClass('active')) {
            $('.capabilites-container h4').next().slideUp(200).removeClass('active');
            $(this).find('h4').next().slideDown(200).addClass('active');
        } else {
            $('.capabilites-container h4').next().slideUp(200).removeClass('active');
        }

        return false;
    });

    // ================================

    $('.page-template-page-home').on('click', '.fa-play', function(){
        document.getElementById('bgvid').pause();
    });

    $('.page-template-page-home').on('click', '.wistia_popover_overlay, .wistia_placebo_close_button', function(){
        document.getElementById('bgvid').play();
    });

    var getActiveHeaderStyle = $('.page-template-page-single-journey .journey-header .columns.active').attr('style');
    $('.page-template-page-single-journey .section-four').attr('style', getActiveHeaderStyle);
    $('.page-template-page-single-journey .section-two .section-two-description .section-two-content p').attr('style', getActiveHeaderStyle);
    $('.page-template-page-single-journey .section-six .button, .page-template-page-single-journey .section-six h2 span').attr('style', getActiveHeaderStyle);

    checkToRemoveBackgroundColourOnText();

    setupJourneyLearnMoreLink();

    var cbpFixedScrollLayout = (function() {

    	// cache and initialize some values
    	var config = {
    		// the cbp-fbscroller´s sections
    		$sections : $('.journey-slider'),
    		// the navigation links
    		$navlinks : $('.journey-slider-container nav:first > a'),
    		// index of current link / section
    		currentLink : 0,
    		// the body element
    		$body : $('html, body'),
    		// the body animation speed
    		animspeed : 650,
    		// the body animation easing (jquery easing)
    		animeasing : 'easeInOutExpo'
    	};

    	function init() {

    		// click on a navigation link: the body is scrolled to the position of the respective section
    		config.$navlinks.on('click', function() {
    			scrollAnim(config.$sections.eq($(this).index()).offset().top + 400);
    			return false;
    		} );

    		// 2 waypoints defined:
    		// First one when we scroll down: the current navigation link gets updated.
    		// A `new section´ is reached when it occupies more than 70% of the viewport
    		// Second one when we scroll up: the current navigation link gets updated.
    		// A `new section´ is reached when it occupies more than 70% of the viewport

            config.$sections.waypoint(function(direction) {
    			if(direction === 'down') {
                    changeNav($(this));
                    $(this).find('.plaxEl').animate({
                        opacity: 1,
                        left: '63%'
                    }, 600);
                } else if(direction === 'up') {
                    $(this).find('.plaxEl').animate({
                        opacity: 0,
                        left: '100%'
                    }, 600);
                }
    		}, {offset: '0%'}).waypoint(function(direction) {
    			if(direction === 'up') {
                    var getID = this.id;
                    getID  = getID.replace('slide-', '') - 1;
                    if(this.id != 'slide-1') {
                        changeNav($('#slide-' + getID));
                    }
                 }
    		}, {offset: '100%'}).waypoint(function(direction) {
                if(direction === 'up') {
                    changeNav($(this));
                    $(this).find('.plaxEl').animate({
                        opacity: 0,
                        left: '100%'
                    }, 600);
                }
            }, {offset: '-30%'});



    		// on window resize: the body is scrolled to the position of the current section
    		$(window).on('debouncedresize', function() {
    			scrollAnim(config.$sections.eq(config.currentLink).offset().top);
    		} );

    	}

    	// update the current navigation link
    	function changeNav($section) {
    		config.$navlinks.eq( config.currentLink ).removeClass( 'cbp-fbcurrent' );
    		config.currentLink = $section.index( '.journey-slider' );
    		config.$navlinks.eq( config.currentLink ).addClass( 'cbp-fbcurrent' );
    	}

    	// function to scroll / animate the body
    	function scrollAnim(top) {
    		config.$body.stop().animate({scrollTop : top}, config.animspeed, config.animeasing, function(){
                // alert();
            });
    	}

    	return {init : init};

    })();

    $(window).load(function() {

        if($(window).width() > 1200) {
            cbpFixedScrollLayout.init();
        }

         // Generate thumbnails from wistia for client videos page
        if ($('body').hasClass('page-template-page-studio-hire')) {
            var wisitaAPIPassword = '098cd3370419348bbef0d709c22459b914b8b4b426e19f4c6862e8b3d5a57708';
            var apiMediaURLS = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15'];
            var apiMediaURL = [];
            var totalVideoJSON = [];
            var finalJSON = [];

            var count = 0;
            $.each(apiMediaURLS, function(key, value) {
                apiMediaURL[key] = 'https://api.wistia.com/v1/medias.json?api_password=' + wisitaAPIPassword + '&page=' + value;
                $.getJSON(apiMediaURL[key], function(result) {
                    count++;
                    totalVideoJSON.push(result);
                    if (count === 15) {
                        totalVideoJSON.map(function(json) {
                            json.map(function(j) {
                                finalJSON.push(j)
                            });
                        });

                        getThumbs(finalJSON);
                    }
                });
            });

            function getThumbs(json) {
                json.map(function(item) {
                    //console.log(item)
                    var wistiaID = item.hashed_id;
                    $('.video-columns').each(function() {
                        var videoID = $(this).data('video-id');
                        if (videoID == wistiaID) {
                            $('.video-thumbnail').addClass('thumbnail-loaded');
                            var thumbnail = item.thumbnail.url;
                            $('.video-columns[data-video-id="' + wistiaID + '"] .video-thumbnail').css('background', 'url(' + removeQueryFromURL(thumbnail) + ') no-repeat center / cover');
                        }
                    });
                })
            }
            
        }   
        
        function removeQueryFromURL(url) {
            return url.split("?")[0];
        }

        $('.webform_step').css("background", "black");

        $('body[data-referer="elearningco"]').find('#referer-overlay').fadeIn(400, function() {
            $('.referer-overlay-content').delay(500).animate({
                top: '50%',
                opacity: 1
            }, 1000);
        });

        $('.close-referer').click(function(){
            $('#referer-overlay').fadeOut(400);
        });

        // Initiate flexsliders and fancybox
        $('.flexslider').flexslider({
            animation: "slide",
            slideshow: false,
            video: true,
            pauseOnAction: true,
            paseOnHover: true,
        });

        $('#carousel').flexslider({
            animation: "slide",
            slideshow: false,
            animationLoop: false,
            itemWidth: 650,
            itemMargin: 0
        });

        $('.fancybox').fancybox({
            protect: true,
            animationEffect: "fade",
            transitionEffect: "slide"
        });

        $('.fancybox-video').fancybox({
            width: "64%",
            height: "70%",
            padding: 0,
            helpers: {
                media: {
                }
            }
        });

        $('.package-contents').on('click', function(){
            $('.package-contents').removeClass('active visible');
            $(this).addClass('active visible');

            var getPackageData = $(this).attr('data-package');
            $('.package-nav li').removeClass('active');
            $('#' + getPackageData).addClass('active');
            

            // fade out arrows if either end of carousel
            switch(true) {
                case $(this).next('.package-contents').length == 0:
                    $('.nav-arrows#next').fadeOut(300);
                    break;
                case $(this).prev('.package-contents').length == 0:
                    $('.nav-arrows#prev').fadeOut(300);
                    break;
                default:
                    $('.nav-arrows').fadeIn(300);
            }
        });

        $('.page-template-page-studio-hire-php .section-two .button').on('click', function(e){
            e.preventDefault();
            $('html, body').animate({
                scrollTop : $('.section-six').offset().top - 90 // -90 for header
            }, 1000);
        });

        
        function calculateTotalPackageWidth() {
            setTimeout(function(){
                var totalPackageWidth = 0;
                $('.package-contents').each(function(key, value) {
                    totalPackageWidth += parseInt($(this).css('width'));     
                }); 
                $('.package-inner-wrapper').css('width', totalPackageWidth);
            }, 500);
        }

        var notDesktop = false;
        if($('body').hasClass('page-template-page-studio-hire-php')) {
            calculateTotalPackageWidth();
            if($(window).width() < 992) {
                notDesktop = true;
                $('#prev').hide();
                $('.package-contents:first-child').addClass('active visible');
                $('.package-contents:not(:first-child)').removeClass('active visible').addClass('not-visible');
            }

            $(window).resize(function() {
                if($(window).width() < 992) {
                    calculateTotalPackageWidth();
                    notDesktop = true;
                    $('#prev').hide();
                    $('#next').show();
                    $('.package-contents').removeAttr('style');
                    $('.package-contents:first-child').removeClass('not-visible').addClass('active visible');
                    $('.package-contents:not(:first-child)').removeClass('active visible').addClass('not-visible');
                    $('.package-nav li').removeClass('active');
                    $('.package-nav li:first-child').addClass('active');
                } else {
                    calculateTotalPackageWidth();
                    notDesktop = false;
                    $('.nav-arrows').show();
                    $('.package-contents').removeAttr('style');
                    $('.package-contents').removeClass('active visible').addClass('not-visible');
                    $('.package-contents:first-child').removeClass('not-visible active visible');
                    $('.package-contents:nth-child(2)').removeClass('not-visible').addClass('active visible');
                    $('.package-contents:nth-child(3)').removeClass('not-visible');
                    $('.package-nav li').removeClass('active');
                    $('.package-nav li:nth-child(2)').addClass('active');
                }
            });
        }
        

        $('.page-template-page-studio-hire-php .nav-arrows').on('click', function(){
            var currentActive = $('.package-contents.active');
            var prevPackage = currentActive.prev('.package-contents');
            var nextPackage = currentActive.next('.package-contents');
            var everyFourthPackage = $('.package-contents:nth-of-type(4n)');
            var packageWidth = parseInt($('.package-contents').css('width'));
            var packageLeft = $('.package-contents').css('left');

            var currentNavItem = $('.package-nav .active');
            var nextNavItem = currentNavItem.next('li');
            var prevNavItem = currentNavItem.prev('li');
        
            // IE returns auto instead of 0px else turn value into integer
            if(packageLeft == 'auto') {
                packageLeft = 0;
            } else {
                packageLeft = parseInt(packageLeft);
            }

            $('.package-contents').removeClass('active visible');
            $('.package-nav li').removeClass('active');

            $(this).addClass('disable');

            // if button is next else prev
            if($(this).is('#next')) {
                $('.nav-arrows#prev').fadeIn(300);
                nextPackage.addClass('active');
                nextNavItem.addClass('active');

                // Hide Next Button if there is no more packages
                if(nextPackage.next('.package-contents').length == 0) {
                    $(this).fadeOut(300);
                }

                // if next package is not visible then animate in else just make next visible
                if(nextPackage.hasClass('not-visible')) {
                    nextPackage.removeClass('not-visible');
                    $('.package-contents').stop().animate({
                        left: packageLeft - packageWidth + 'px'
                    }, 300, function() {
                        if(notDesktop) {
                            currentActive.addClass('not-visible');
                        } else {
                            prevPackage.prev('.package-contents').addClass('not-visible');
                        }
                        setTimeout(function(){
                            $('.nav-arrows').removeClass('disable');
                        }, 700);
                        nextPackage.addClass('visible');                                           
                    });
                } else {
                   nextPackage.addClass('visible'); 
                   $('.nav-arrows').removeClass('disable');
                }
                
            } else {
                
                $('.nav-arrows#next').fadeIn(300);
                prevPackage.addClass('active');
                prevNavItem.addClass('active');

                // Hide Prev Button if there is no more packages
                if(prevPackage.prev('.package-contents').length == 0) {
                    $(this).fadeOut(300);
                } 

                // if prev package is not visible then animate in else just make prev visible
                if(prevPackage.hasClass('not-visible')) {
                    prevPackage.removeClass('not-visible');
                    $('.package-contents').stop().animate({
                        left: packageLeft + packageWidth + 'px'
                    }, 300, function() {
                        if(notDesktop) {
                            currentActive.addClass('not-visible');
                        } else {
                            nextPackage.next('.package-contents').addClass('not-visible');
                        }
                        setTimeout(function(){
                            $('.nav-arrows').removeClass('disable');
                        }, 700);
                        prevPackage.addClass('visible');                                         
                    });
                } else {
                   prevPackage.addClass('visible'); 
                   $('.nav-arrows').removeClass('disable');
                }

            }
            
        });
    });


    var getOriginalHeight;
    $('.crew-detail-wrapper').on('click', function(){
        $(this).css('pointer-events', 'none');
        var crewDetails = $(this).find('.crew-details');
        if(crewDetails.hasClass('active')) {
            crewDetails.removeClass('active');
            crewDetails.find('.crew-description').animate({
                opacity: 0,
                top: '20%'
            }, 300, function(){
                $(this).parents('.crew-detail-wrapper').css('pointer-events', 'visible');
                $(this).hide();
                $(this).parent().animate({
                    height: getOriginalHeight
                }, 300, function(){
                    $(this).find('.fa').removeClass('fa-angle-down').addClass('fa-angle-up');
                });
            });

        } else {
            getOriginalHeight = crewDetails.css('height');
            crewDetails.addClass('active').animate({
                height: '100%'
            }, 300, function(){
                $(this).parents('.crew-detail-wrapper').css('pointer-events', 'visible');
                crewDetails.find('.fa').removeClass('fa-angle-up').addClass('fa-angle-down');
                crewDetails.find('.crew-description').show().animate({
                    opacity: 1,
                    top: 0
                }, 300);
            });
        }

    });

    /***************************** CLIENT VIDEOS SCRIPTS *************************/

    var priceFilter;
    var qsRegex;
    var searchItemsFilter = '';

    var $grid = $('#videos-wrapper > .row').isotope({
        itemSelector: '.video-columns',
        layoutMode: 'fitRows',
        filter: function() {
            // return price, search and search items result. Show if true
            var priceResult = priceFilter ? $(this).is(priceFilter) : true;
            var searchResult = qsRegex ? $(this).find('.client-title').text().match( qsRegex ) : true;
            var searchItemsResult = searchItemsFilter ? $(this).is(searchItemsFilter) : true;
            return priceResult && searchResult && searchItemsResult;
        }
    });

    $grid.on('arrangeComplete', function(){
        $('#cant-find').hide();
        // check if all videos are hidden
        if($('.video-columns:visible').length === 0) {
            $('#cant-find').fadeIn(300);
        }
    });

    $.fn.digits = function(){
        return this.each(function(){
            $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
        });
    };

    var priceSlider = $('#price-slider').slider({
        min: 50000,
        max: 300000,
        range: true,
        values: [50000, 300000],
        step: 10000,
        slide: function(event, ui) {
            $('#minprice').val("$" + ui.values[0].toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ","));
            $('#maxprice').val("$" + ui.values[1].toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ","));
        },
        stop: function(event, ui) {
            $('#cant-find').hide();
            var minPrice = ui.values[0];
            var maxPrice = ui.values[1];

            // check price slider
            $('.video-columns').each(function(){
                var projectCost = $(this).data('cost');
                console.log(projectCost, minPrice, maxPrice);
                if(projectCost >= minPrice && projectCost <= maxPrice) {
                    $(this).addClass('in-between-price');
                } else {
                    $(this).removeClass('in-between-price');
                }
            });

            // if not videos are visible set filter to show nothing
            if($('.video-columns.in-between-price').length === 0) {
                priceFilter = '.nothing';
            } else {
                priceFilter = '.in-between-price';
            }

            $grid.isotope();

        }
    });

    $("#minprice").val("$" + $("#price-slider").slider("values", 0).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ","));
    $("#maxprice").val("$" + $("#price-slider").slider("values", 1).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ","));

    $('.items-wrapper').on('click', '.items', function(){
        $('#cant-find').hide();
        $(this).toggleClass('selected');

        if($(this).hasClass('selected')) {
            searchItemsFilter = searchItemsFilter += $(this).data('filter');
        } else {
            searchItemsFilter = '';
            $('.items-wrapper .items.selected').each(function(index){
                searchItemsFilter = searchItemsFilter += $(this).data('filter');
            });
        }

        console.log(searchItemsFilter);
        $grid.isotope();
    });


    // use value of search field to filter
    var search = $('#search-client-project').keyup( debounce( function() {
        qsRegex = new RegExp(search.val(), 'gi');
        $('#cant-find').hide();
        $grid.isotope();
    }, 300 ) );



    $(window).load(function(){
        $('#videos-wrapper').animate({
            opacity: 1
        }, 500);
        $('.client-videos-search-container').fadeIn(500);

        var queries = {};
        // check if url has a query
        if(window.location.href.indexOf('?') > -1) {
            // get query from url
            $.each(document.location.search.substr(1).split('&'), function(c,q){
                var i = q.split('=');
                queries[i[0].toString()] = i[1].toString();
            });

            // check each video to see if video name matches up with url query name then show video
            $('.video-columns').each(function(){
                var getVideoName = $(this).data('video-name');
                if(getVideoName == queries.video) {
                    console.log('in');
                    $('.client-videos-overlay').fadeIn(300);
                    $(this).find('.wisita-video-wrapper').fadeIn(300);
                    $('.close-video').remove();
                    $('html, body').css('overflow', 'hidden');
                    $('.client-videos-search-container, .video-thumbnail-wrapper').css('pointer-events', 'none');
                }
            });
        }

    });


    $('.video-columns .copy').each(function(index){
        var clipboard = new Clipboard($('.copy')[index]);

        clipboard.on('success', function(e){
            console.info('Text:', e.text, e);
            setTooltip('Copied!', e.trigger);
            hideTooltip();
        });
    });

    $('.video-columns .copy').tooltip();

    // Client Videos on click show overlay
    $('.video-thumbnail-content').on('click', function(e){
        
        var clone = $(this).parent().clone();
        if($('.active-project .video-columns').length > 0) {
            $('.active-project .video-columns').remove();
            clone.appendTo('.active-project');
        } else {
            clone.appendTo('.active-project');
        }
        
        $('.active-project').slideDown(500, function(){
            $('html, body').animate({
                scrollTop: $('.active-project').offset().top - 180
            }, 300);
        });
    });

    $('.active-project').on('click', '.close-project', function(){
        $('.active-project').slideUp(500, function(){
            $('.active-project .video-columns').remove();
        });
    });

    $('.our-work-link a').on('click', function() {
        $('html, body').animate({
            scrollTop: $('#our-work').offset().top - 120
        }, 500);
    });

    $('.close-video').on('click', function(){
        $('.client-videos-overlay').fadeOut(300);
        $('.wisita-video-wrapper').fadeOut(300);
    });

    $('.category-btns').on('click', function(){
        $('.items-wrapper').hide();
        $('.category-btns').removeClass('selected');
        $(this).addClass('selected');

        var getData = $(this).data('show');
        switch(getData) {
            case 'project-type':
                $('#project-type-items').show();
                break;
            case 'capability':
                $('#capability-items').show();
                break;
        }
    });


    function setTooltip(message, element) {
        $(element).attr('data-original-title', message).tooltip('show');
    }

    function hideTooltip() {
        setTimeout(function() {
            $('.copy').tooltip('hide').attr('data-original-title', 'Copy Link');
        }, 1000);
    }

    function debounce( fn, threshold ) {
        var timeout;
        return function debounced() {
            if ( timeout ) {
                clearTimeout( timeout );
            }
            function delayed() {
                fn();
                timeout = null;
            }
            timeout = setTimeout( delayed, threshold || 100 );
        };
    }

    /***************************** END CLIENT VIDEOS SCRIPTS *************************/






    // Get height of single journey image and match assign height to content to center text
    var singleJourneyImgHeight = $('.page-template-page-single-journey-php .section-two-image img').css('height');
    $('.page-template-page-single-journey-php .section-two-description').css('height', singleJourneyImgHeight);


    $('.page-template-page-crew .crew-image-container').hover(function() {
        $(this).find('.crew-image').animate({
            opacity: 0
        }, 400);
        $(this).find('.crew-image-hover').animate({
            opacity: 1
        }, 200);
    }, function() {
        $(this).find('.crew-image').animate({
            opacity: 1
        }, 200);
        $(this).find('.crew-image-hover').animate({
            opacity: 0
        }, 400);
    });

    // This will run even if the page doesn't fully load. It used to be in the $(window).bind() function below,
    //  but this seems to work just as well.
    $('.section-one .parallax-content').animate({
        left: "50%",
        opacity: 1
    }, 1500);

    $('.single-project .parallax-content h3, .single-project .parallax-content h1, .single-project .parallax-content .journey-details').animate({
        opacity: 1,
        left: '1px'
    }, 1000);

    // Run after every other js has loaded
    // NOTE: This isn't working, and I'm not exactly sure why. The slideup seems to happen instantly, and regardless of whether that function is even called.
    $('.menu-item-has-children').mouseenter(function($e){
        $(this).find('.sub-menu').slideDown(200);
    });
    $('.menu-item-has-children').mouseleave(function($e){
        $(this).find('.sub-menu').slideUp(200);
    });

    $('.page-template-page-projects .down-arrow-link').click(function(){
        var goToNext = $(this).parents('.parallax-window').next();
        $("html, body").animate({
            scrollTop: $(goToNext).offset().top - 90 // -90 cause of the header
        }, 1000);
    });

    /*$('.page-template-page-journey .down-arrow-link').click(function(){
        var goToNext = $(this).parents('.journey-slider').next();
        $("html, body").animate({
            scrollTop: $(goToNext).offset().top - 90 // -90 cause of the header
        }, 1000);
    });*/

    $('.page-template-page-journey .section-three .fa').click(function(){

        if($(window).width() > 1200) {
            $("html, body").animate({
                scrollTop: $('.page-template-page-journey .section-four').offset().top + $('.page-template-page-journey .section-four').outerHeight() - window.innerHeight + 250
            }, 800);
        } else {
            $("html, body").animate({
                scrollTop: $('.page-template-page-journey .section-four').offset().top - 80
            }, 800);
        }
    });

    // ===================================================
    // Journey page code
    // ===================================================
    if($('body').hasClass('page-template-page-journey')) {
        $('.section-four').css('display', 'block');
        adjustWindow();
    }

    // ===================================================
    // Individual journey code
    // ===================================================

    //Change journey pages via load function without refreshing the page
    $('.journey-header .columns a').on('click', function(event) {
        changeJourney($(this), event);
    });

    // ===================================================
    // Scrolling
    // ===================================================

    $(window).scroll(onScroll);
    onScroll(); // This isn't always necessary, but it's good for when it is

    // ===================================================
    // Squeeze page code
    // ===================================================

    // Hide the popup when the x button is clicked
    $('.close-popup').on("click", function (e) {
        closePopup($('.popup'));
    });

    // Replace the text on the buttons
    if (document.getElementById("div-containing-url"))
        $('.wpcf7-submit').attr("value", document.getElementById("div-containing-url").getAttribute("data-submit-text"));

    // AJAX ==============================================
    $("input").on("change", function() {
         $(this).attr("value", $(this).val());
    });

    $('.wpcf7-submit').click( function (e) {
        e.preventDefault();

        $( '.wpcf7-not-valid-tip').remove();

        var $form = $(this).parents("div.wpcf7 > form");

        checkToSendDataToClui($form, function() {
            // Go directly into the wpcf7 plugin
            wpcf7.submit( $form );
        });
    });

    // ===================================================
    // Blog post code
    // ===================================================
    // If we haven't already closed the popup, or signed up, then show the popup.
    if (checkSignupPopupCookie()) {
        $('.blog .popup').delay(2500).animate({
            opacity: 1
        });
    } else {
        $('.blog .popup').css("display", "none");
    }

    // On click signup submit button
    $('.wfe_button').on('click', function(){
        if($('body').hasClass('single-post')) {
            localStorage.setItem('single-post-url', window.location.href);
        }
    });

    // Get Query From URL
    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for(var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if(pair[0] == variable) {
               return pair[1];
            }
        }
       return(false);
    }

    if(getQueryVariable('signup')) {
        var getURL = localStorage.getItem('single-post-url');
        if(getURL !== null) {
            window.location.href = getURL + '?signup=thankyou';
            localStorage.removeItem('single-post-url');
        } else {
            $('html, body').animate({
                scrollTop: $('.signup-container').offset().top - 200
            }, 800);
            $('.signup-container').html('<h3 class="text-center thanks-signup">Thanks for signing up!</h3>');
        }

        if (checkSignupPopupCookie())
        {
            setCookie("doNotShowSignupPopup", "true", 30);
            $('.blog .popup').css("opacity", "1");
            setTimeout(function() {
                closePopup($('.blog .popup'));
            }, 2500);
        }
    }

    // ===================================================
    // Safeguarding against Internet Explorer's shenanigans
    // ===================================================
    if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i) ){
        $("html").addClass("ie");

        // Disabling the parallax in the project page
        if ($(".page-template-page-projects").length)
        {
            $(".parallax-window").each(function () {
                $(this).attr("data-parallax", "");
                $(this).css("background-image", "url(" + $(this).attr("data-image-src") + ")");
            });
        }
    }

});

function checkSignupPopupCookie() {
    return getCookie("doNotShowSignupPopup") == null || getCookie("doNotShowSignupPopup") == "false";
}

function changeJourney( $clickedElement, event ) {
    // Change the highlighted journey and set that as the style we should be paying attention to
    $('.journey-header .columns').removeClass('active');
    $clickedElement.parent().addClass('active');
    var getActiveHeaderStyle = $clickedElement.parent().attr('style');

    event.preventDefault(); // Stop us from actually changing page

    var getURL = $clickedElement.attr('href');

    // ===============================================================
    // Scroll to the top
    var getWindowWidth = $(window).width();
    var isTop = $(window).scrollTop();
    if(isTop > 0) {
        $("html, body").animate({
            scrollTop: $('html').offset().top
        }, 500);
    }
    if(getWindowWidth < 768) {
        $("html, body").animate({
            scrollTop: $('.sections-container').offset().top - 100
        }, 1000);
    }
    // ===============================================================

    // Fade out the content while we switch over
    $('.sections-container, .parallax-mirror').animate({
        'opacity': 0
    }, 200);

    // Load in the content from the given URL and then put it into .sections-container, then do what's in the function
    $('.sections-container').load(getURL + ' .sections-container', function(data){
        // Fade it back in
        $('.sections-container').animate({
            'opacity': 1
        }, 200);

        // Colour text according to what our active style is
        $('.page-template-page-single-journey .section-four').attr('style', getActiveHeaderStyle);
        $('.page-template-page-single-journey .section-two .section-two-description .section-two-content p').attr('style', getActiveHeaderStyle);
        $('.page-template-page-single-journey .section-six .button, .page-template-page-single-journey .section-six h2 span').attr('style', getActiveHeaderStyle);

        checkToRemoveBackgroundColourOnText();
        setupJourneyLearnMoreLink();

        // Change the URL for the purpose of when someone goes back, they should be retrieving this information
        var pageName = getURL.replace(/.+choose-your-journey\//, '').replace(/\//, '');
        var stateObj = { page: pageName };

        history.pushState(stateObj, pageName, '/choose-your-journey/' + pageName);
    });
}

function new_map( $el ) {

	// var
	var $markers = $el.find('.marker');


	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};


	// create map
	var map = new google.maps.Map( $el[0], args);


	// add a markers reference
	map.markers = [];


	// add markers
	$markers.each(function(){

    	add_marker( $(this), map );

	});


	// center map
	center_map( map );


	// return
	return map;

}

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

// =============================================================================================
// Source: https://stackoverflow.com/questions/5968196/check-cookie-if-cookie-exists
function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }

    return decodeURI(dc.substring(begin + prefix.length, end));
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
// =============================================================================================

// Used for the blog sign up form
function closePopup( $el ) {
    $el.animate({
        opacity: 0
    }, {
        duration: 800,
        easing: 'swing',
        queue: false,
        complete: function () {
            $('.popup').css("display", "none");
        }
    });

    if ($('.blog').length)
        setCookie("doNotShowSignupPopup", "true", 30);
}

// Called whenever we load a single-journey page, or we swap to a different single-journey page.
function setupJourneyLearnMoreLink() {
    $('.page-template-page-single-journey .section-six .button').click(function(){
        $('html, body').animate({
            scrollTop: $('.contact-form').offset().top
        }, 1000);
    });
}

// =============================================================================================
// Clui user account creation
// =============================================================================================

function isEmail(email) {
    // Retrieved from: http://emailregex.com/
  var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}

function isPhoneNumber(number) {
  var regex = /[0-9]+/;
  return regex.test(number) && number.length >= 10;
}

function customValidation($form) {
    return  $form.parent().find('[name="your-name"]').val() != '' &&
            isEmail($form.parent().find('[name="your-email"]').val()) &&
            isPhoneNumber($form.parent().find('[name="your-number"]').val());
}

function getIfWeShouldRedirectToClui() {
    if ($("#div-containing-url").length > 0)
        return $("#div-containing-url").attr("data-redirecting-to-clui");
}

function getUrlRedirectingToClui() {
    var $divContainingURL = document.getElementById("div-containing-url");
    return $divContainingURL.getAttribute("data-redirect-url");
}

// Called in the Contact Form 7 scripts.js file before we submit a form
function checkToSendDataToClui( $form, onFinish ) {
    // I don't like how we aren't using the same rules to validate the form in POST as just to send to Clui.
    //  It opens up the potential for something to pass one and not the other.
    if (getIfWeShouldRedirectToClui() == 1 && customValidation( $form )) {
        sendDataToClui(getUrlRedirectingToClui(), $form, onFinish);
    }
    else {
        onFinish();
    }

}

// Called in the squeeze page when we want to create a user on Clui
// Note that a new wpcf7 form will need to be made for each squeeze page which is made.
function sendDataToClui( $url, $activeForm, onFinish ) {
    if ($activeForm) {
        $('.please-wait-overlay').css("display", "block");
        $('.please-wait-overlay').animate({
            opacity: 1
        }, {
            duration: 1000,
            easing: 'swing',
            queue: false,
            complete: function () {
                setupCF7eventListeners();

                // Once the overlay has faded in
                var $fullName = $activeForm.find('[name="your-name"]');
                var $mobileNumber = $activeForm.find('[name="your-number"]');
                var $email = $activeForm.find('[name="your-email"]');

                var nameString = $fullName.val();
                var names = nameString.split(" ");
                var firstName = names[0];

                // This technically means users with multiple first names will lose some of them, but it shouldn't matter to Clui.
                names.splice(0, 1);
                var lastName = names.join(" ");

                var spacelessMobileNumber = $mobileNumber.val().replace(/\s/g, '');
                $url += '/people';

                var jsonObj = {
                    "username": $email.val(),
                    "email": $email.val(),
                    "firstName": firstName,
                    "lastName": lastName,
                    "password": spacelessMobileNumber,
                    "sendEmails": false,
                    "activated": true
                };
                $.ajax({
                    type: 'POST',
                    url: $url,
                    //async: false,
                    dataType: 'json',
                    // contentType: "application/json; charset=utf-8",
                    data: jsonObj,
                    success: function(data) {
                        loginToken(data.data.id, onFinish, $activeForm, $url);
                    },
                    error: function(err) {
                        console.log(err);

                        var $message = $( '.wpcf7-response-output', $activeForm );
                        $message.addClass( 'wpcf7-validation-errors' );
                        $activeForm.addClass( 'invalid' );

                        $message.html( '' ).append( "That email already exists in our demo database. Please enter a different email address." ).slideDown( 'fast' );
                        $message.attr( 'role', 'alert' );

                        $( '.ajax-loader', $activeForm ).removeClass( 'is-active' );

                        onCF7mailFailed();
                    }
                });

            }
        });
    }
    else
    {
        console.log("Active form not defined!");
    }
}

// Generates a login token for a user to immediately log in to clui with
// NOTE: If there are different
function loginToken(id, onFinish, form, $url) {
    $.ajax({
        type: 'POST',
        url: $url+id+"/createLoginToken",
        // async: false,
        success: function(data) {
            cluiLink = data.data.link;
            form.find('[name="login-token"]').attr("value", cluiLink);
            $('#please-wait-text').html("Not long now! We're emailing your login details...");
            onFinish();
        },
        error: function(err) {
            console.log(err);
        }
    });
}

function setupCF7eventListeners() {
    document.addEventListener( 'wpcf7mailsent', onCF7mailSent );
    document.addEventListener( 'wpcf7mailfailed', onCF7mailFailed );
    document.addEventListener( 'wpcf7invalid', onCF7mailFailed );
    document.addEventListener( 'wpcf7spam', onCF7mailFailed );
}

var cluiLink;
function onCF7mailSent() {
    $('#please-wait-text').html("Let's go!");
    window.location.href = cluiLink;
}

function onCF7mailFailed() {
    $('.please-wait-overlay').animate({
        opacity: 0
    }, {
        duration: 1000,
        easing: 'swing',
        queue: false,
        complete: function () {
            $('.please-wait-overlay').css("display", "none");
        }
    });
}

// =============================================================================================

$(window).on('resize', adjustWindow);

function adjustWindow(){
    if($('body').hasClass('page-template-page-journey')) {
        if($(window).width() > 1200) {
            // get window size
            winH = $(window).height();
            if(winH <= 550) {
                winH = 550;
            }
            $('.journey-slider').height(winH*2);

            var totalSliderHeight = 0;
            $('.journey-slider').each(function(){
                totalSliderHeight += $(this).height();
            });

            var getHeaderHeight = parseInt($('#masthead').css('height'));
            totalSliderHeight = totalSliderHeight + getHeaderHeight;

            //console.log(totalSliderHeight, skrollr.init().getMaxScrollTop());

            //if($(window).width() > 1200) {
                var getVisibleJourneyContentHeight = parseInt($('#journey-visible-content').css('height'));
                var updatedSliderHeight = totalSliderHeight - getVisibleJourneyContentHeight;
                $('#journey-hidden-content').css('top', updatedSliderHeight + 'px');
                $('#visible-footer-content').css('top', updatedSliderHeight + 'px');
                $('footer').css('top', updatedSliderHeight + 'px');
            //}


        } else {
            $('#journey-hidden-content, #visible-footer-content, .page-template-page-journey .site-footer').css('top', 0);
        }
    }
}

function onScroll() {
    //console.log('is scrolling');
    enableScroll();
    var time;
    var timeText;
    var getWindowWidth = $(window).width();
    scrolling = document.body.scrollTop || document.documentElement.scrollTop;
    // console.log(scrolling);

    // =============================================================================================
    // Choose your journey slides IE fix
    // =============================================================================================
    if ($('.ie').length) {
        // Set parallax overlay's opacities to their parents in IE
        if ($('.page-template-page-journey').length) {
            $('.parallax-overlay').each(function() {
                $(this).css('opacity', $(this).parents('.journey-slider').children('.plaxEl').css('opacity'));
            });
        }
    }

    // =============================================================================================
    // Animation stuff
    // =============================================================================================

    if(getWindowWidth > 767) {
        if(scrolling > 30) {
            $('body').addClass('scroll');
        } else {
            $('body').removeClass('scroll');
        }
    }

    if(scrolling > 100 && $(window).width() > 992) {
        $('.scroll-btn').fadeIn(200);
    }
    else {
        $('.scroll-btn').fadeOut(200);
    }

    // Show animations for each section

    // Run animation when scrolling hits every 700 inbetween a remainer of 0 and 50

    $('.page-template-page-projects .section-one-loop .parallax-window').each( function (key, element) {
        var position = $(this).position();

        if (scrolling > position.top - ($(this).height() / 2)){
            // If we are scrolling past a certain point, rush the animations
            if (scrolling < position.top + ($(this).height() / 5)) {
                $(this).find(".col-xs-12:last-child img").animate({
                    opacity: 1
                }, 1500);
                $(this).find(".section-one-content").delay(500).animate({
                    left: "1px",
                    opacity: 1
                }, 1200);
                $(this).find(".section-one-content .button").delay(1000).animate({
                    left: "1px",
                    opacity: 1
                }, 1000);
            }
            else
            {
                $(this).find(".col-xs-12:last-child img").stop(true, true);
                $(this).find(".col-xs-12:last-child img").animate({
                    opacity: 1
                }, 100);

                $(this).find(".section-one-content").stop(true, true);
                $(this).find(".section-one-content").dequeue();
                $(this).find(".section-one-content").delay(50).animate({
                    left: "1px",
                    opacity: 1
                }, 400);

                $(this).find(".section-one-content .button").stop(true, true);
                $(this).find(".section-one-content .button").dequeue();
                $(this).find(".section-one-content .button").delay(100).animate({
                    left: "1px",
                    opacity: 1
                }, 300);
            }
        }
    });


    if(scrolling > 300) {
        $('.home .section-two h3').animate({
            right: "1px",
            opacity: 1
        }, 1500);
    }

    if(scrolling > 390) {
        time = 500;
        $('.page-template-page-single-journey-php .section-three .crew-container .col-md-2').each(function(key, value){
            setTimeout(function(){
                $(value).find('img').animate({
                    left: "1px",
                    opacity: 1
                }, 500, function(){
                    $(value).find('span').animate({
                        opacity: 1
                    }, 500);
                    $(value).addClass('fadeIn');
                });
            }, time);
            time += 500;
        });

        time = 0;
        $('.page-template-page-adhoc .key-points').each(function(key, value){
            setTimeout(function(){
                $(value).animate({
                    opacity: 1
                }, 700, function(){
                    $(value).addClass('animation-complete');
                });
            }, time);
            time += 700;
        });
    }

    if(scrolling > 400) {
        time = 500;
        $('.home .process-steps').each(function(){
            $(this).delay(time).animate({
                opacity: 1,
            }, time);
            time += 500;
        });
        setTimeout(function() {
            $('.home #croomo-icon').fadeIn(500);
            $('.home .section-three .description').delay(500).animate({
                left: '1px',
                opacity: 1
            }, 1000);
            $('.home .section-three .button').delay(1000).animate({
                top: '1px',
                opacity: 1
            }, 1000);
        }, 2500);
    }

    if (scrolling >= 1000 ) {
        // Rush the .process-steps animations
        $('.home .process-steps').each(function(){
            $(this).stop(true, true);
            $(this).animate({
                opacity: 1,
            }, 200);
        });

        $('.home #croomo-icon').stop(true, true);
        $('.home #croomo-icon').fadeIn(100);

        $('.home .section-three .description').stop(true, true);
        $('.home .section-three .description').delay(50).animate({
                left: '1px',
                opacity: 1
            }, 300);

        $('.home .section-three .button').stop(true, true);
        $('.home .section-three .button').delay(100).animate({
                top: '1px',
                opacity: 1
            }, 300);
    }

    if(scrolling > 550) {
        $('#pathfinder-section h1').animate({
            left: "1px"
        }, 500);
    }

    if(scrolling > 680) {
        $('.page-template-page-what-we-do .section-two img').animate({
            left: "1px",
            opacity: 1
        }, 1000);

        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .col-xs-12:last-child img').animate({
            opacity: 1
        }, 1500);
        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content').delay(500).animate({
            left: "1px",
            opacity: 1
        }, 1200);
        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content .button').delay(1000).animate({
            left: "1px",
            opacity: 1
        }, 1000);

        $('.page-template-page-what-we-do .section-two .icons').each(function(){
            $(this).delay(time).animate({
                opacity: 1
            }, 500);
            var $this = $(this);
            setTimeout(function(){
                $this.addClass('active');
            }, time);
            time += 500;

        });
    }

    if(scrolling > 1458) {
        $('.page-template-page-what-we-do .section-two img').stop(true, true);
        $('.page-template-page-what-we-do .section-two img').animate({
            left: "1px",
            opacity: 1
        }, 200);

        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .col-xs-12:last-child img').stop(true, true);
        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .col-xs-12:last-child img').animate({
            opacity: 1
        }, 300);

        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content').stop(true, true);
        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content').dequeue();
        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content').delay(50).animate({
            left: "1px",
            opacity: 1
        }, 240);

        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content .button').stop(true, true);
        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content .button').dequeue();
        $('.page-template-page-projects .section-one-loop .parallax-window:first-child .parallax-content .section-one-content .button').delay(100).animate({
            left: "1px",
            opacity: 1
        }, 200);

        $('.page-template-page-what-we-do .section-two .icons').each(function(){
            $(this).stop(true, true);
            $(this).dequeue();
            $(this).delay(time).animate({
                opacity: 1
            }, 100);
            var $this = $(this);
            setTimeout(function(){
                $this.addClass('active');
            }, time);
            time += 50;
        });
    }

    if(scrolling > 800) {
        $('.single-project .crew-details').each(function(){
            $(this).delay(time).animate({
                opacity: 1,
                left: "1px"
            }, 200);
            time += 200;
        });
    }

    if(scrolling > 200) {
        time = 0;
        $('.timeline-journey').each(function(key, value){
            setTimeout(function(){
                $(value).find('img').fadeIn(200);
                $(value).find('h2').delay(100).animate({
                    opacity: 1,
                    right: '1px'
                }, 500);
                $(value).find('h4').delay(200).animate({
                    opacity: 1,
                    right: '1px'
                }, 500);
                $(value).find('p').delay(300).animate({
                    opacity: 1,
                    right: '1px'
                }, 500, function(){
                    $(value).addClass('animate-in');
                });
            }, time);
            time += 1000;
        });
    }

    if(scrolling > 1100) {
        $('#trailblazer-section h1').animate({
            left: "1px"
        }, 500);

        time = 0;
        $('.page-template-page-crew .crew-details').each(function(){
            $(this).delay(time).animate({
                opacity: 1,
                left: "1px"
            }, 200);
            time += 200;
        });
    }


    if(scrolling > 1650) {
        $('.section-four .crew-container span').animate({
            opacity: 1
        }, 1000);
        $('#pioneer-section h1').animate({
            left: "1px"
        }, 500);
    }

    if(scrolling > 2000) {
        $('#icon-1').animate({
            opacity: 1
        }, 1000);
        setTimeout(function(){
            $('#icon-1').addClass('active');
            setTimeout(function(){
                $('#icon-2').animate({
                    opacity: 1
                }, 1000);
                setTimeout(function(){
                    $('#icon-2').addClass('active');
                    setTimeout(function(){
                        $('#icon-3').animate({
                            opacity: 1
                        }, 1000);
                        setTimeout(function(){
                            $('#icon-3').addClass('active');
                            setTimeout(function(){
                                $('#icon-4').animate({
                                    opacity: 1
                                }, 1000);
                            }, 300);
                        }, 300);
                    }, 300);
                }, 300);
            }, 300);
        }, 300);
    }

    if(scrolling > 2275) {
        $('.page-template-page-single-journey .section-six .parallax-content h3, .page-template-page-single-journey .section-six .parallax-content h1').animate({
            opacity: 1,
            left: '1px'
        }, 1000);
        $('.page-template-page-single-journey .section-six .parallax-content .button').animate({
            opacity: 1,
            left: '1px'
        }, 1000);
    }

    // Home page
    if(scrolling > 2600) {
        $('.section-seven .logo-img img').addClass('active');
        $('.page-template-page-what-we-do .section-six-img').animate({
            opacity: 1,
            right: "1px"
        }, 1000);
    }

    // Squeeze page
    var $popupAppeared = false;

    if($('.page-template-page-squeeze').length > 0)
    {
        if(scrolling > $('.section-four').position().top - $(window).height()) {
            $('.section-four .logo-img img').addClass('active');
        }

        if(scrolling > $('.section-five').position().top - ( $('.section-five').height() * 0.8) && !$popupAppeared)
        {
            $popupAppeared = true;
            $('.section-five .popup').delay(500).animate({
                opacity: 1
            });
        }
    }

    if(scrolling > 3500) {


        var timeCultureImage = 300;
        timeText = 300;

        $('.page-template-page-crew .culture-img, .page-template-page-crew-2 .culture-img').each(function(){
            $(this).delay(timeCultureImage).animate({
                opacity: 1,
                left: "1px"
            }, 500);
            $(this).parent().find('.culture-text').delay(timeText).animate({
                opacity: 1,
                right: "1px"
            }, 500);
            timeCultureImage += 300;
            timeText += 300;
        });
    }

    if(scrolling > 2250) {
        var timeAwardImg = 300;
        timeText = 300;

        $('.page-template-page-what-we-do .award-img').each(function(){
            $(this).delay(timeAwardImg).animate({
                opacity: 1,
                left: "1px"
            }, 500, function(){
                $(this).addClass('flipped');
            });
            $(this).parent().find('.award-text').delay(timeText).animate({
                opacity: 1,
                right: "1px"
            }, 500);
            timeAwardImg += 300;
            timeText += 300;
        });
    }

    if(scrolling > 2950) {
        time = 200;
        $('.awards-timeline ul li').animate({
            opacity: 1,
            right: "1px"
        }, 500);
        $('.awards-timeline ul li').each(function(key, value){
            setTimeout(function(){
                $(value).addClass('active');
            }, time);
            time += 200;
        });
    }

    if(scrolling > 5150) {
        $('.section-ten h1').animate({
            opacity: 1
        }, 1000);

        setTimeout(function(){
            $('.section-ten .contact-container').animate({
                opacity: 1
            }, 1000);
        }, 700);
    }

    if(scrolling > 5600) {
        $('.page-template-page-what-we-do .section-nine h1').animate({
            opacity: 1
        }, 1000);

        setTimeout(function(){
            $('.page-template-page-what-we-do .section-nine .contact-container').animate({
                opacity: 1
            }, 1000);
        }, 700);
    }

    if($('.contact-form').length > 0) {
        if(scrolling > $('.contact-form').position().top - 1000){
            $('.contact-form h1').animate({
                opacity: 1
            }, 1000);

            setTimeout(function(){
                $('.contact-container').animate({
                    opacity: 1
                }, 1000);
            }, 700);
        }
    }
}

function checkToRemoveBackgroundColourOnText() {
    if( $('.page-template-page-what-we-do').length > 0 ) {
        var $backgroundText = $('.page-template-page-what-we-do .section-two h1');
        $paragraphTextFillColor = $backgroundText.css('-webkit-text-fill-color');

        if ( getIfColorIsTransparent($paragraphTextFillColor)) {
            $backgroundText.css('background', 'none');
        }
    }

    // If we are on a non-webkit supported browser, then we have to reset the background part of this styling as it won't work with -webkit-text-fill-color: transparent
    $paragraphTextFillColor = $('.page-template-page-single-journey .section-two .section-two-description .section-two-content p').css('-webkit-text-fill-color');
    if ( getIfColorIsTransparent($paragraphTextFillColor))
    {
        // If I wanted to change the color of the text, uncomment the lines and get the appropriate colour from the header
        $('.page-template-page-single-journey .section-two .section-two-description .section-two-content p').css('background', 'none');
        $('.page-template-page-single-journey .section-six .button span, .page-template-page-single-journey .section-six h2 span').css('background', 'none');
        // $('.page-template-page-single-journey .section-two .section-two-description .section-two-content p').css('color', 'blue');
        // $('.page-template-page-single-journey .section-six .button, .page-template-page-single-journey .section-six h2 span').css('color', 'blue');
    }
}

function getIfColorIsTransparent( color ) {
    return color != 'rgba(0, 0, 0, 0)' && color != 'transparent';
}
