<?php
/**
 * Template part for displaying single projects.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package croomo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <!-- SECTION ONE -->
    <section class="section-one" style="<?php echo (get_field('single_project_background_video') ? 'height: 605px;' : ''); ?>">
        <?php
            $getParallaxImage = get_field('main_project_bg_image');
            $getJourneyIcon = get_field('main_project_journey_icon');
        ?>
        <div class="parallax-window text-center" data-bleed="1" data-parallax="scroll" data-image-src="<?php echo $getParallaxImage['url']; ?>">
            <?php if(get_field('single_project_background_video')):
            $getVideo = get_field('single_project_video'); ?>
            <video playsinline autoplay muted loop poster="<?php echo $getParallaxImage['url']; ?>" id="bgvid">
                <source src="<?php echo $getVideo['url']; ?>" type="<?php echo $getVideo['mime_type']; ?>">
            </video>
            <?php endif; ?>
            <div class="parallax-content">
                <h1><?php the_title(); ?></h1>
                <h3><?php the_field('project_info_sub_heading'); ?></h3>
                <div class="journey-details text-center">
                    <img src="<?php echo $getJourneyIcon['url']; ?>" alt="<?php echo $getJourneyIcon['alt']; ?>" title="<?php echo $getJourneyIcon['alt']; ?>" />
                    <span>Journey: <?php the_field('project_info_journey'); ?></span>
                </div>
            </div>
        </div>

    </section>
    <!-- END SECTION ONE -->

    <!-- SECTION TWO -->
    <section class="section-two" style="<?php echo(!get_field('single_project_gallery') && !get_field('single_project_description_image_2') ? 'padding-bottom: 80px' : ''); ?>">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1 class="text-center"><?php the_field('single_project_title') ?></h1>
				</div>
				<div class="col-xs-12">
                    <?php
						$descriptionImage1 = get_field('single_project_description_image_1');
                        $descriptionImage2 = get_field('single_project_description_image_2');
					?>
                    <div class="section-two-description">
                        <?php if($descriptionImage1): ?>
                        <img class="description-image1" src="<?php echo $descriptionImage1['url']; ?>" title="<?php echo $descriptionImage1['alt']; ?>" alt="<?php echo $descriptionImage1['alt']; ?>" />
                        <?php
                        endif;
                        the_field('single_project_description');
                        if(get_field('single_project_wistia_video_id')): ?>
                        <div class="wistia-video">
                            <script src="//fast.wistia.com/embed/medias/<?php the_field('single_project_wistia_video_id'); ?>.jsonp" async></script>
                            <script src="//fast.wistia.com/assets/external/E-v1.js" async></script>
                            <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
                                <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                                    <div class="wistia_embed wistia_async_<?php the_field('single_project_wistia_video_id'); ?> videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if($descriptionImage2 && !get_field('single_project_gallery')): ?>
                    <img class="description-image2" src="<?php echo $descriptionImage2['url']; ?>" title="<?php echo $descriptionImage2['alt']; ?>" alt="<?php echo $descriptionImage2['alt']; ?>" />
                    <?php endif; ?>
                </div>
			</div>
		</div>
        <?php if(get_field('single_project_gallery')): ?>
        <div class="container-fluid gallery">
            <div class="row">
                <?php
                $images = get_field('single_project_project_gallery');
                if( $images ): ?>
                    <div id="carousel">
                        <ul class="slides">
                            <?php foreach( $images as $image ): ?>
                                <li>
                                    <a class="fancybox" rel="croomo-gallery" href="<?php echo $image['url']; ?>">
                                        <div class="project-gallery-images" style="background: url(<?php echo $image['url']; ?>) no-repeat center center / cover;"></div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
	</section>
    <!-- END SECTION TWO -->

    <!-- SECTION THREE -->
    <?php if(get_field('main_project_testimonial_quote')): ?>
    <section class="section-three">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="testimonial-text">
                        <?php the_field('main_project_testimonial_quote'); ?>
                    </div>
                    <?php if(get_field('single_project_testimonial_wistia_video_id')): ?>
                    <div class="testimonial-wistia-video">
                        <script src="//fast.wistia.com/embed/medias/<?php the_field('single_project_testimonial_wistia_video_id'); ?>.jsonp" async></script>
                        <script src="//fast.wistia.com/assets/external/E-v1.js" async></script>
                        <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
                            <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                                <div class="wistia_embed wistia_async_<?php the_field('single_project_testimonial_wistia_video_id'); ?> videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- END SECTION THREE -->

    <!-- SECTION FIVE -->
    <?php if(get_field('single_project_capabilities')): ?>
    <section class="section-five">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1><?php the_field('single_project_capabilities_title'); ?></h1>
                    <div class="crew-involved-container">
                        <?php
                        $capabilities = get_field('single_project_capabilities');
        				if($capabilities):
                            foreach($capabilities as $index => $cb):
                                $ID = $cb->ID;
                                $capabilityIcon = get_field('capabilities_icon', $ID); ?>
        						<div class="col-xs-12 col-sm-6 col-md-3 text-center capabilities-container">
                                    <img src="<?php echo $capabilityIcon['url']; ?>" title="<?php echo $capabilityIcon['alt']; ?>" alt="<?php echo $capabilityIcon['alt']; ?>" class="crew-image" />
                                    <h3><?php echo get_the_title($ID); ?></h3>
                                </div>
    	                    <?php
                            endforeach;
                        endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 text-center">
                    <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'What We Do' ) ) ).'#add-on-capabilities'; ?>" class="button purple"><?php the_field('single_project_capabilities_button_text'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!-- END SECTION FIVE -->

    <!-- SECTION SIX -->
    <section class="section-six">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1><?php the_field('single_project_similar_projects_title'); ?></h1>
                    <div class="similar-projects-container text-center">
                        <?php
                        $similarProjects = get_field('single_projects_similar_projects');
                        $count = 1;

            			if($similarProjects):
            				foreach($similarProjects as $sp): ?>
                                <?php
                                switch(count($similarProjects)){
                                    case 1:
                                        echo '<div class="col-xs-12 col-sm-4 col-sm-offset-4 projects">';
                                        break;
                                    case 2:
                                        echo '<div class="col-xs-12 col-sm-6 projects">';
                                        break;
                                    case 3:
                                        echo '<div class="col-xs-12 col-sm-4 projects">';
                                        break;
                                    case 4:
                                        echo '<div class="col-xs-12 col-sm-3 projects">';
                                        break;
                                }
                                ?>
            						<?php
            							$ID = $sp->ID;
            							$spThumbnail = get_field('project_info_homepage_thumbnail', $ID);
            						?>
            						<a href="<?php the_permalink($ID); ?>">
            							<img class="project-thumbnail" src="<?php echo $spThumbnail['url']; ?>" alt="<?php echo $spThumbnail['alt']; ?>" title="<?php echo $spThumbnail['alt']; ?>" />
            							<div class="project-overlay">
            								<div class="project-overlay-content">
            									<h3><?php echo get_the_title($ID); ?></h4>
            									<h4><?php the_field('project_info_sub_heading', $ID); ?></h5>
            									<h5>Journey: <?php the_field('project_info_journey', $ID); ?></h6>
            								</div>
            							</div>
            						</a>
            					</div>
			                <?php
                            $count++;
            				endforeach;
            			endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 text-center">
                    <a href="<?php the_field('single_project_similar_projects_button_link'); ?>" class="button green"><?php the_field('single_project_similar_projects_button_text'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION SIX -->

    <!-- PROJECTS FOOTER -->
    <?php
        $getBGImage = get_field('project_footer_background_image', 'option');
        $getImage = get_field('project_footer_image', 'option');
    ?>
    <section class="projects-footer" style="background: url(<?php echo $getBGImage['url']; ?>) no-repeat center center / cover;">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="lime-green"><?php the_field('project_footer_title', 'option'); ?></h1>
                    <div class="project-footer-image-container">
                        <a href="<?php the_field('single_project_image_link', 'option'); ?>"><img src="<?php echo $getImage['url']; ?>" /></a>
                        <h3 class="lime-green"><?php the_field('project_footer_contact_us_text', 'option'); ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- PROJECTS FOOTER -->

</article><!-- #post-## -->
