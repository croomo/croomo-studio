<?php defined( 'ABSPATH' ) or die;
/**
 * @package Vision6 Web Forms
 */

/*
Plugin Name: Vision6
Description: Easily add Vision6 web forms to your website using a widget or shortcode to grow your lists.
Version: 1.0.1
Author: Vision6
Author URI: http://www.vision6.com.au/custom/vision6/click.php
License: GPLv2 or later
*/

defined('WEBFORMS_BRANDING') or define('WEBFORMS_BRANDING', 'Vision6');
defined('WEBFORMS_SYSTEM_URL') or define('WEBFORMS_SYSTEM_URL', 'https://www.vision6.com.au');
defined('WEBFORMS_BRAND_WEBSITE') or define('WEBFORMS_BRAND_WEBSITE', 'http://www.vision6.com.au/custom/vision6/click.php');
defined('WEBFORMS_PLUGIN_PATH') or define('WEBFORMS_PLUGIN_PATH', plugin_dir_path(__FILE__));
defined('WEBFORMS_IFRAME_RESIZER_SCRIPT') or define('WEBFORMS_IFRAME_RESIZER_SCRIPT', WEBFORMS_SYSTEM_URL . '/js/third_party/iframe_resizer/iframeResizer.min.js');
defined('WEBFORMS_INC_PATH') or define('WEBFORMS_INC_PATH', WEBFORMS_PLUGIN_PATH.'inc/');
defined('WEBFORMS_TEMPLATE_PATH') or define('WEBFORMS_TEMPLATE_PATH', WEBFORMS_PLUGIN_PATH.'tpl/');
defined('WEBFORMS_TEMPLATE') or define('WEBFORMS_TEMPLATE', 'webforms_iframe.php');

require_once(WEBFORMS_INC_PATH . 'plugin.php');