<?php
/**
 * Template part for displaying results in page-contact.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package croomo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <!-- SECTION ONE -->
    <?php
        $BGImage = get_field('section_one_background_image_cu');
    ?>
    <section class="section-one" style="background: url(<?php echo $BGImage['url']; ?>) no-repeat center top / cover">
        <div class="container">
            <div class="row section-one-content">
                <div class="col-xs-12">
                    <h1 class="lime-green"><?php echo the_title(); ?></h1>
                    <p><?php the_field('section_one_description_cu'); ?></p>
                </div>
            </div>
            <div class="row section-one-details">
                <div class="col-xs-12 col-md-4">
                    <?php
    				if(have_rows('section_contact_details_cu')):?>
                    <ul>
    					<?php
                        while(have_rows('section_contact_details_cu')) : the_row(); ?>
    						<li>
    							<div class="section-one-details-content">
                                    <span class="pull-left lime-green"><?php the_sub_field('section_one_icon_cu'); ?></span>
                                    <p class="pull-left"><?php the_sub_field('section_one_details_cu'); ?></p>
                                </div>
    						</li>
	                    <?php
                        endwhile; ?>
                    </ul>
    				<?php endif; ?>
                </div>
                <div class="col-xs-12 col-md-8">
                    <?php echo do_shortcode(get_field('section_one_contact_form_cu')); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- END SECTION ONE -->

    <!-- SECTION TWO -->
    <section class="section-two">
        <div class="container-fluid">
            <?php
                $getMap = get_field('section_two_map_cu');
            ?>
            <div class="acf-map">
            	<div class="marker" data-lat="<?php echo $getMap['lat']; ?>" data-lng="<?php echo $getMap['lng']; ?>"></div>
            </div>
        </div>
    </section>
    <!-- END SECTION TWO -->

</article><!-- #post-## -->
