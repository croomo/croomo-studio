<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package croomo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				<?php if ( 'post' === get_post_type() ) : ?>
				<div class="meta-data">
					<span class="entry-meta pull-left">Posted on <?php the_time('F jS, Y') ?></span>
					<?php
					$getCrewMembers = get_field('blog_author');
					if($getCrewMembers):
						foreach($getCrewMembers as $index => $member): ?>
						<span class="pull-left author">&nbsp;|&nbsp;<?php echo $member->post_title; ?></span>
						<?php
						endforeach;
					endif; ?>

				</div>
				<?php endif; ?>
				<div class="featured-image">
					<?php the_post_thumbnail(); ?>
				</div>
				<div class="entry-content pull-left">
					<?php
					if(is_home()) {
						echo wp_trim_words( get_the_content(), 30, '...' );
					} else {
						the_content();
					}
					?>
				</div><!-- .entry-content -->
				<?php if(is_home()): ?>
				<a href="<?php the_permalink(); ?>" class="pull-left button purple text-center">Read More</a>
				<?php endif; ?>
			</div>
		</div>
		<?php if(!is_home()): ?>
		<div class="row blog-author">
			<?php
			$getCrewMembers = get_field('blog_author');
			if($getCrewMembers):
				foreach($getCrewMembers as $index => $member):
					$ID = $member->ID;
					$crewImage = get_field('crew_member_image', $ID);
					$crewHoverImage = get_field('crew_member_image_hover', $ID); ?>
					<div class="col-xs-12 col-sm-3 text-center crew-detail">
						<div class="crew-detail-wrapper">
							<div class="crew-image-container">
								<img src="<?php echo $crewImage['url']; ?>" title="<?php echo $crewImage['alt']; ?>" alt="<?php echo $crewImage['alt']; ?>" class="crew-image" />
								<img src="<?php echo $crewHoverImage['url']; ?>" title="<?php echo $crewHoverImage['alt']; ?>" alt="<?php echo $crewHoverImage['alt']; ?>" class="crew-image-hover" />
							</div>
							<div class="crew-details">
								<h4 class="crew-name"><?php echo $member->post_title; ?></h4>
								<span class="crew-title"><?php the_field('crew_member_title', $ID); ?></span>
							</div>
						</div>
					</div>
				<?php
				endforeach;
			endif; ?>
		</div>
		<?php endif; ?>
	</div>

</article><!-- #post-## -->
