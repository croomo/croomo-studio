<?php
/**
 * Template part for displaying results in page-crew.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package croomo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <!-- SECTION ONE -->
    <section class="section-one">
        <?php
        $sectionOneImage = get_field('section_one_parallax_image_sh');
        $getVideo = get_field('section_one_parallax_video_sh'); ?>
        <div class="parallax-window container-fluid text-center" data-bleed="0" data-parallax="scroll" data-image-src="<?php echo $sectionOneImage['url']; ?>">
            <div class="parallax-overlay" style="background: rgba(0, 0, 0, 0.5) url(<?php echo  get_template_directory_uri(); ?>/images/sh-bg-overlay.png) repeat top left;"></div>
            <video playsinline autoplay muted loop poster="<?php echo $getParallaxImage['url']; ?>" id="bgvid">
                <source src="<?php echo $getVideo['url']; ?>" type="<?php echo $getVideo['mime_type']; ?>">
            </video>
            <div class="parallax-content">
                <h1><?php the_field('section_one_parallax_heading_sh'); ?></h1>
                <?php if(get_field('section_one_parallax_description_sh')) : ?>
                    <p><?php the_field('section_one_parallax_description_sh'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <!-- END SECTION ONE -->

    <!-- SECTION TWO -->
    <section class="section-two">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3><?php the_field('section_two_title_sh'); ?></h3>
                    <div class="description">
                        <?php the_field('section_two_description_sh'); ?>
                    </div>
                    <a href="" class="button green"><?php the_field('section_two_button_text_sh'); ?></a>
                </div>  
            </div>
        </div>
    </section>
    <!-- END SECTION TWO-->

    <!-- SECTION THREE -->
    <section class="section-three">
        <div class="pan">
            <?php 
            $panoramaImg = get_field('section_three_panorama_image_sh'); ?>
            <a-scene>
                <a-sky src="<?php echo $panoramaImg['url']; ?>" rotation="0 -85 -10"></a-sky>
            </a-scene>
            <div class="pan-overlay" style="background: rgba(0, 0, 0, 0.5) url(<?php echo  get_template_directory_uri(); ?>/images/sh-bg-overlay.png) repeat top left;"></div>
        </div>
        <div class="pan-instructions text-center">
            <i class="fa fa-arrows" aria-hidden="true"></i>
            <h4><?php the_field('panorama_instruction'); ?></h4>
        </div>       
    </section>
    <!-- END SECTION THREE -->

    <!-- SECTION FOUR -->
    <section class="section-four">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3><?php the_field('section_four_title_sh'); ?></h3>
                    <div class="description">
                        <?php the_field('section_four_description_sh'); ?>
                    </div>
                    <a href="<?php the_field('section_four_button_link_sh'); ?>" class="button" target="_blank"><?php the_field('section_four_button_text_sh'); ?></a>
                </div>  
            </div>
        </div>
    </section>
    <!-- END SECTION FOUR -->

    <!-- SECTION FIVE -->
    <div class="section-five">
        <div class="container-fluid">
            <div class="row">
                <div class="gallery-container">
                    <?php 
                    if(have_rows('section_five_gallery_images_sh')):
                        while(have_rows('section_five_gallery_images_sh')) : the_row();
                        $galleryImage = get_sub_field('section_five_gallery_image_sh'); ?>
                        <div class="images col-xs-12 col-sm-6 col-md-4">
                            <a href="<?php echo $galleryImage['url']; ?>" class="fancybox" data-fancybox="sh-gallery">
                                <span style="background: url(<?php echo $galleryImage['url']; ?>) no-repeat center / cover;" /></span>
                            </a>
                        </div>
                        <?php
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div> 
    </div>
    <!-- END SECTION FIVE -->

    <!-- SECTION SEVEN -->
    <div id="our-work" class="section-seven">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1><?php the_field('section_seven_title'); ?></h1>
                    <div class="active-project"></div>
                    <?php 

                    $posts = get_field('section_seven_projects');
                    
                    if( $posts ): ?>
                        
                        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT)
                            $videoID = get_field('project_video_id');?>
                            <div class="video-columns col-xs-12 col-md-6" data-video-id="<?php echo $videoID; ?>">
                                <div class="video-thumbnail-content">
                                    <div class="video-thumbnail"></div>
                                    <div class="overlay">
                                        <h2><?php echo $post->post_title; ?></h2>
                                    </div>
                                </div>
                                <div class="video-contents">
                                    <div class="project-video">
                                        <span class="close-project">
                                            <i class="fa fa-close"></i>
                                        </span>
                                        <script src="https://fast.wistia.com/embed/medias/<?php echo $videoID; ?>.jsonp" async></script>
                                        <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
                                            <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                                                <div class="wistia_embed wistia_async_<?php echo $videoID; ?> videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="video-contents-wrapper">
                                        <h2><?php echo the_field('project_title'); ?></h2>
                                        <div class="description">
                                            <?php echo the_field('project_description'); ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        <?php endforeach; ?>
                        
                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION SEVEN -->

    <!-- SECTION SIX -->
    <div class="section-six">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1><?php the_field('section_six_title_sh'); ?></h1>
                    <div class="description">
                        <?php the_field('section_six_description_sh'); ?>
                    </div>
                    <div class="package-container">
                        <div class="package-wrapper">
                            <div class="package-inner-wrapper">
                                <?php 
                                if(have_rows('section_six_packages_sh')):
                                    $count = 1;
                                    while(have_rows('section_six_packages_sh')) : the_row(); ?>
                                            <div class="package-contents <?php echo ($count == 2 ? 'active visible' : '') . '' . ($count > 3 ? 'not-visible' : ''); ?>" data-package="package-<?php echo $count; ?>">
                                                <div class="package-header">
                                                    <div>
                                                        <h4><?php the_sub_field('section_six_package_title_sh'); ?></h4>
                                                        <div class="package-description">
                                                            <?php the_sub_field('section_six_package_description_sh'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                if(have_rows('section_six_package_options_sh')):
                                                    while(have_rows('section_six_package_options_sh')) : the_row(); ?>
                                                        <div class="package-options">
                                                            <div class="package-options-wrapper">
                                                                <span class="option-description"><?php the_sub_field('section_six_package_options_option_text_sh'); ?></span>
                                                                <span class="option-price"><?php the_sub_field('section_six_package_options_option_price_sh'); ?></span>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    endwhile;
                                                endif; ?>
                                                <a href="<?php the_sub_field('section_six_package_button_link_sh'); ?>" class="button" target="_blank"><?php the_sub_field('section_six_package_button_text_sh'); ?>
                                                </a>
                                            </div>
                                        <?php 
                                        $count++;
                                    endwhile;
                                endif; ?>
                            </div>
                            <div class="package-arrows">
                                <div id="prev" class="nav-arrows">
                                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                </div>
                                <div id="next" class="nav-arrows">
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="package-nav text-center">
                            <ul>
                                <?php 
                                $packages = get_field('section_six_packages_sh');
                                foreach($packages as $index => $p): 
                                    $index = $index + 1; ?>
                                    <li id="package-<?php echo $index; ?>" class="<?php echo($index == 2 ? 'active' : '');?>"><span></span></li>
                                    <?php
                                endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION SIX -->

</article><!-- #post-## -->
